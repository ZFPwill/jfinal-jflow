package cn.jflow.common.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 模仿Spring上下文获取的代码，与ContextHandler共同组成
 * <p>
 * Title: ContextUtil.java
 * </p>
 * <p>
 * Description:
 * </p>
 * 
 * @author 张清磊
 * @QQ 544188838
 * @date 2018年10月25日
 */
public class ContextUtil {

	private static final ThreadLocal<ContextUtil> tl = new ThreadLocal<ContextUtil>();

	private final HttpServletRequest request;
	private final HttpServletResponse response;

	private ContextUtil(HttpServletRequest request, HttpServletResponse response) {
		if (request == null || response == null) {
			throw new IllegalArgumentException("request and response can not be null");
		}
		this.request = request;
		this.response = response;
	}
	
	public static void set(HttpServletRequest req, HttpServletResponse resp) {
		tl.set(new ContextUtil(req, resp));
	}

	public static ContextUtil get() {
		return tl.get();
	}

	public static void remove() {
		tl.remove();
	}

	public static HttpServletRequest getRequest() {
		return tl.get().request;
	}

	public static HttpServletResponse getResponse() {
		return tl.get().response;
	}

	public static HttpSession getSession() {
		return tl.get().request.getSession();
	}
	
	public static HttpSession getSession(boolean isCreate) {
		return tl.get().request.getSession(isCreate);
	}
	
	/**
	 * Return a Object from session.
	 * @param key a String specifying the key of the Object stored in session
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getSessionAttr(String key) {
		HttpSession session = getSession(false);
		return session != null ? (T)session.getAttribute(key) : null;
	}
	
	/**
	 * Store Object to session.
	 * @param key a String specifying the key of the Object stored in session
	 * @param value a Object specifying the value stored in session
	 */
	public static void setSessionAttr(String key, Object value) {
		getSession(true).setAttribute(key, value);
	}
	
	/**
	 * Remove Object in session.
	 * @param key a String specifying the key of the Object stored in session
	 */
	public static void removeSessionAttr(String key) {
		HttpSession session = getSession(false);
		if (session != null)
			session.removeAttribute(key);
	}

	public static void addCookie(String name, int expiry, String value) {
		if (getRequest() != null) {
			Cookie cookies[] = getRequest().getCookies();
			if (cookies == null) {
				Cookie cookie = new Cookie(name, value);
				cookie.setMaxAge(expiry);
				getResponse().addCookie(cookie);
			} else {
				for (Cookie cookie : cookies) {
					if (name.equals(cookie.getName())) {
						cookie.setValue(value);
						cookie.setMaxAge(expiry);
						getResponse().addCookie(cookie);
						return;
					}
				}
				Cookie cookie = new Cookie(name, value);
				cookie.setMaxAge(expiry);
				getResponse().addCookie(cookie);
			}
		}
	}
	
	public static Cookie getCookie(String name) {
		Cookie cookies[] = getRequest().getCookies();
		if (cookies == null || name == null || name.length() == 0)
			return null;
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName())) {
				return cookie;
			}
		}
		return null;
	}

	public static void deleteCookie(String name) {
		Cookie cookies[] = getRequest().getCookies();
		if (cookies == null || name == null || name.length() == 0)
			return;
		for (Cookie cookie : cookies) {
			if (name.equals(cookie.getName())) {
				cookie.setValue("");
				cookie.setMaxAge(0);
				getResponse().addCookie(cookie);
			}
		}
	}

	public static void clearCookie() {
		Cookie[] cookies = getRequest().getCookies();
		if (null == cookies)
			return;
		for (Cookie cookie : cookies) {
			cookie.setValue("");
			cookie.setMaxAge(0);
			getResponse().addCookie(cookie);

		}
	}
}
