package cn.jflow.init;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.kit.PropKit;
import com.jfinal.template.Engine;

import cn.jflow.controller.app.CreatCascaController;
import cn.jflow.controller.app.SaveCommController;
import cn.jflow.controller.app.SearchCascaController;
import cn.jflow.controller.appace.AppACE_Controller;
import cn.jflow.controller.ccmobile.CCMobileController;
import cn.jflow.controller.ccmobile.CCMobile_CCFormController;
import cn.jflow.controller.ccmobile.CCMobile_MyFlowController;
import cn.jflow.controller.ccmobile.CCMobile_RptSearchController;
import cn.jflow.controller.ccmobile.CCMobile_SettingController;
import cn.jflow.controller.ccmobile.CCMobile_WorkOptController;
import cn.jflow.controller.ccmobile.CCMobile_WorkOpt_OneWorkController;

public class JflowConfig extends JFinalConfig {

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(PropKit.use("jflow.properties").getBoolean("config.devMode"));
		me.setEncoding("UTF-8");
	}

	@Override
	public void configRoute(Routes me) {
		me.add("/App", CreatCascaController.class);
		me.add("/App", SaveCommController.class);
		me.add("/App", SearchCascaController.class);
		me.add("/AppACE", AppACE_Controller.class);
		me.add("/CCMobile/CCForm", CCMobile_CCFormController.class);
		me.add("/CCMobile/MyFlow", CCMobile_MyFlowController.class);
		me.add("/CCMobile/RptSearch", CCMobile_RptSearchController.class);
		me.add("/CCMobile/Setting", CCMobile_SettingController.class);
		me.add("/CCMobile/WorkOpt/OneWork", CCMobile_WorkOpt_OneWorkController.class);
		me.add("/CCMobile/WorkOpt", CCMobile_WorkOptController.class);
		me.add("/CCMobile", CCMobileController.class);
	}

	@Override
	public void configEngine(Engine me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configPlugin(Plugins me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub
		
	}

}
