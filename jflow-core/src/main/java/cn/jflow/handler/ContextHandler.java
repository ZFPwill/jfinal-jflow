package cn.jflow.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jfinal.handler.Handler;

import cn.jflow.common.util.ContextUtil;

public class ContextHandler extends Handler{

	@Override
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		ContextUtil.set(request, response);
		next.handle(target, request, response, isHandled);
	}
}
