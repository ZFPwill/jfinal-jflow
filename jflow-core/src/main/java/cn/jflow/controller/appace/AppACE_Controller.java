package cn.jflow.controller.appace;

import java.io.IOException;
import java.io.PrintWriter;

import com.jfinal.core.ActionKey;

import BP.Tools.StringHelper;
import BP.WF.HttpHandler.AppACE;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class AppACE_Controller extends HttpHandlerBase{
	
   /**
    * 默认执行的方法
    * @return 
    */
	@ActionKey("/ProcessRequest")
    public final void ProcessRequestPost()
    {
		AppACE  appACEHandler = new AppACE();
    	super.ProcessRequest(appACEHandler);
    }
    
    @Override
    public Class<AppACE> getCtrlType() {
    	return AppACE.class;
    }
}
