package cn.jflow.controller.ccmobile;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.ActionKey;

import BP.WF.HttpHandler.CCMobile;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class CCMobileController extends HttpHandlerBase {
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@ActionKey("/ProcessRequest")
	public final void ProcessRequestPost(HttpServletRequest request) 
	{
		CCMobile  AttrHandler = new CCMobile();
		super.ProcessRequest(AttrHandler);
	}
	@Override
	public Class <CCMobile>getCtrlType() {
		return CCMobile.class;
	}
}

