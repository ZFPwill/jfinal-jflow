package cn.jflow.controller.ccmobile;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.ActionKey;

import BP.WF.HttpHandler.CCMobile_RptSearch;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class CCMobile_RptSearchController extends HttpHandlerBase {
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@ActionKey("/ProcessRequest")
	public final void ProcessRequestPost(HttpServletRequest request) 
	{
		CCMobile_RptSearch  AttrHandler = new CCMobile_RptSearch();
		super.ProcessRequest(AttrHandler);
	}
	@Override
	public Class <CCMobile_RptSearch>getCtrlType() {
		return CCMobile_RptSearch.class;
	}
}

