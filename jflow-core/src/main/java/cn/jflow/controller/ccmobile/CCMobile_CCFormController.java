package cn.jflow.controller.ccmobile;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.ActionKey;

import BP.WF.HttpHandler.CCMobile_CCForm;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class CCMobile_CCFormController extends HttpHandlerBase {
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@ActionKey("/ProcessRequest")
	public final void ProcessRequestPost(HttpServletRequest request) 
	{
		CCMobile_CCForm  AttrHandler = new CCMobile_CCForm();
		super.ProcessRequest(AttrHandler);
	}
	
	
	@Override
	public Class <CCMobile_CCForm>getCtrlType() {
		return CCMobile_CCForm.class;
	}
}

