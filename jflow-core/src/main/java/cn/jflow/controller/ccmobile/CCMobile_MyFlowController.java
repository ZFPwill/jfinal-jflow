package cn.jflow.controller.ccmobile;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.ActionKey;

import BP.WF.HttpHandler.CCMobile_MyFlow;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class CCMobile_MyFlowController extends HttpHandlerBase {
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@ActionKey("/ProcessRequest")
	public final void ProcessRequestPost(HttpServletRequest request) 
	{
		CCMobile_MyFlow  AttrHandler = new CCMobile_MyFlow();
		super.ProcessRequest(AttrHandler);
	}
	@Override
	public Class <CCMobile_MyFlow>getCtrlType() {
		return CCMobile_MyFlow.class;
	}
}

