package cn.jflow.controller.ccmobile;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.ActionKey;

import BP.WF.HttpHandler.CCMobile_WorkOpt;
import BP.WF.HttpHandler.Base.HttpHandlerBase;

public class CCMobile_WorkOptController extends HttpHandlerBase {
	/**
	 * 默认执行的方法
	 * 
	 * @return
	 */
	@ActionKey("/ProcessRequest")
	public final void ProcessRequestPost(HttpServletRequest request) 
	{
		CCMobile_WorkOpt  AttrHandler = new CCMobile_WorkOpt();
		super.ProcessRequest(AttrHandler);
	}
	@Override
	public Class <CCMobile_WorkOpt>getCtrlType() {
		return CCMobile_WorkOpt.class;
	}
}

